package main.solutions

import main.abstracts.ExampleFramework

/**
 * @author Erick Pranata
 * @since 2019-06-24
 *
 */
class ExampleSolution : ExampleFramework {
    override fun solution(input: IntArray): Int {
        return input.sumBy { it }
    }
}